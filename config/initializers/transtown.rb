# frozen_string_literal: true
module Mastodon
  module Version
    module_function
    def repository
      'queersocial/mastodon'
    end

    def flags
      'queersocial'
    end

    def source_base_url
      'https://github.com/queersocial/mastodon'
    end
  end
end
